import React, { Component } from "react";
import PropTypes from "prop-types";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import FormControl from "@material-ui/core/FormControl";  
import Input from "@material-ui/core/Input";
import InputLabel from "@material-ui/core/InputLabel";
import LockIcon from "@material-ui/icons/LockOutlined";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import withStyles from "@material-ui/core/styles/withStyles";
import axios from 'axios';
import {reqURL} from '../../../../config/reqURL';

const styles = theme => ({
  main: {
    width: "auto",
    display: "block", // Fix IE 11 issue.
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
      width: 400,
      marginLeft: "auto",
      marginRight: "auto"
    }
  },
  paper: {
    marginTop: theme.spacing.unit * 8,
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme
      .spacing.unit * 3}px`
  },
  avatar: {
    margin: theme.spacing.unit,
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing.unit
  },
  submit: {
    marginTop: theme.spacing.unit * 3
  }
});

class Login extends Component {
  state  = {
    isLoggedIn: false,
    isFormValid: false,
    formControls: {
      username: {
        value: "",
        type: "text",
        label: "username",
        errorMessage: "Գրեք ճիշտ էլ. հասցե",
        valid: false,
        touched: false,
        validation: {
          required: true,
          email: true
        }
      },
      password: {
        value: "",
        type: "password",
        label: "Գաղտնաբառ",
        errorMessage: "Գրեք ճիշտ գաղտնաբառ",
        valid: false,
        touched: false,
        validation: {
          required: true,
          minLength: 6
        }
      }
    }
  }

  validateControl(value, validation) {
    if(!validation) {
      return true
    }

    let isValid = true;
    if(validation.required) {
      isValid = value.trim() !== '' && isValid
    }

    
    if(validation.minLength) {
      isValid = value.length >= validation.minLength && isValid
    }

    return isValid;
  }

  onChangeHandler = (event, controlName) => {
    const formControls = {...this.state.formControls};
    const control = {...formControls[controlName]};

    control.value = event.target.value;
    control.touched = true;
    control.valid = this.validateControl(control.value, control.validation);

    formControls[controlName] = control ;

    let isFormValid = true;
    Object.keys(formControls).forEach(name => {
      isFormValid = formControls[name].valid && isFormValid
    })

    this.setState({
      formControls, isFormValid
    }) 
  };
  
  loginHandler = async event => {
    event.preventDefault(); 
    const data = {
      username: this.state.formControls.username.value,     
      password: this.state.formControls.password.value    
    }
    
    try {
       const response = await axios.post(reqURL.adminLogin, data)        
       localStorage.setItem('token',  response.data.token) 
       console.log(response.status)
       if(response.status == 200) {
         this.setState({
           isLoggedIn: true
         })         
        this.props.history.push("/admin")
      }  else {
         console.log(response.data)        
       }
    } catch (error) {
        console.log(error)
    } 
  }
   
  render() {
    const { classes } = this.props;
    return (
      <main className={classes.main}>
        <CssBaseline />
        <Paper className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LockIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Sign in
          </Typography> 
            <FormControl margin="normal" required fullWidth>
              <InputLabel htmlFor="username">username</InputLabel>
              <Input 
              onChange={event => this.onChangeHandler(event, 'username')} 
              value={`${this.state.formControls.username.value}`} 
              valid={`${this.state.formControls.username.valid}`}
              touched={`${this.state.formControls.username.touched}`}
              shouldvalidate={`${!!(this.state.formControls.username.validation)}`}              
              id="username" 
              name="username" 
              autoComplete="username" 
              autoFocus />
            </FormControl>
            <FormControl margin="normal" required fullWidth>
              <InputLabel htmlFor="password">Password</InputLabel>
              <Input
                onChange={event => this.onChangeHandler(event, 'password')} 
                value={`${this.state.formControls.password.value}`}
                valid={`${this.state.formControls.password.valid}`}
                touched={`${this.state.formControls.password.touched}`}
                shouldvalidate={`${!!(this.state.formControls.password.validation)}`}
                name="password"
                type="password"
                id="password"
                autoComplete="current-password"
              />
            </FormControl>
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit} 
              onClick={this.loginHandler}
            >
              Sign in
            </Button> 
             
        </Paper>
      </main>
    );
  }
}

Login.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(Login);
