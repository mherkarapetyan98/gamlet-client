import React from 'react';
import { Link } from 'react-router-dom';  
import CreateIcon from '@material-ui/icons/Create'; 
import ListIcon from '@material-ui/icons/List';  
import MessageIcon from '@material-ui/icons/Message';
// import ServicesIcon from '@material-ui/icons/direction_car'

import { MenuItem, ListItemIcon, ListItemText  } from '@material-ui/core';

 

export const mainListItems = (
  <div>
    <MenuItem component={Link} to="/admin/create">
      <ListItemIcon> 
        <CreateIcon />
      </ListItemIcon>
      <ListItemText  primary="Create Product" />
    </MenuItem>


    <MenuItem component={Link} to="/admin/products">
      <ListItemIcon>
        <ListIcon />
      </ListItemIcon>
      <ListItemText primary="Product list" />
    </MenuItem>   

    <MenuItem component={Link} to="/admin/messages">
      <ListItemIcon> 
        <MessageIcon/>
      </ListItemIcon>
      <ListItemText primary="Message" />
    </MenuItem>   
    <MenuItem component={Link} to="/admin/reviews">
      <ListItemIcon> 
        <MessageIcon/>
      </ListItemIcon>
      <ListItemText primary="Отзывы" />
    </MenuItem>   
    <MenuItem component={Link} to="/admin/services">
      <ListItemIcon> 
         
      </ListItemIcon>
      <ListItemText primary="Services" />
    </MenuItem>   
  </div>
);

 