<TextField
              label="Category"
              margin="normal"
              defaultValue={values.category.value}
              onChange={this.handleChange("category")}
            />

            <input
              accept="image/*"
              className={classes.input}
              id="contained-button-file"
              onChange={this.handleChange("imageSrc")}
              type="file"
            />
            <label htmlFor="contained-button-file">
              <Button
                variant="contained"
                component="span"
                className={classes.button}
                fullWidth
              >
                Upload image
              </Button>
            </label>

            <TextField
              label="Size"
              margin="normal"
              defaultValue={values.size.value}
              onChange={this.handleChange("size")}
            />

            <TextField
              label="Height"
              margin="normal"
              defaultValue={values.height.value}
              onChange={this.handleChange("height")}
            />

            <TextField
              label="process"
              margin="normal"
              defaultValue={values.process.value}
              onChange={this.handleChange("process")}
            />

            <TextField
              label="price"
              margin="normal"
              defaultValue={values.price.value}
              onChange={this.handleChange("price")}
            />

            <TextField
              label="priceColor"
              margin="normal"
              defaultValue={values.priceColor.value}
              onChange={this.handleChange("priceColor")}
            />

 
import React, { Component } from "react";
import { Grid, InputAdornment, TextField, Button } from "@material-ui/core";
import classNames from "classnames";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";

const styles = theme => ({
  root: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    flexWrap: "wrap"
  },
  margin: {
    margin: theme.spacing.unit
  },
  input: {
    display: "none"
  }
});

class CreateProduct extends Component {
  state = {
    isFormValid: false,
    formControls: {
      name: {
        value: "",
        valid: false,
        touched: false,
        validation: {
          required: true
        }
      },
      category: {
        value: "",
        valid: false,
        touched: false,
        validation: {
          required: true
        }
      },
      imageSrc: {
        value: "",
        valid: false,
        touched: false,
        validation: {
          required: true
        }
      },
      size: {
        value: "",
        valid: false,
        touched: false,
        validation: {
          required: true
        }
      },
      height: {
        value: "",
        valid: false,
        touched: false,
        validation: {
          required: true
        }
      },
      process: {
        value: "",
        valid: false,
        touched: false,
        validation: {
          required: true
        }
      },
      price: {
        value: "",
        valid: false,
        touched: false,
        validation: {
          required: true
        }
      },
      priceColor: ""
    }
  };

  validateControl(value, validation) {
    if(!validation) {
      return true
    }

    let isValid = true;
    if(validation.required) {
      isValid = value.trim() !== '' && isValid
    }
   

    return isValid;
  }

  handleChange = (event, controlName) => {
    const formControls = {...this.state.formControls};
    const control = {...formControls[controlName]};
    
    control.value = event.target.value;

    control.touched = true;
    control.valid = this.validateControl(control.value, control.validation);

    formControls[controlName] = control ;
    let isFormValid = true;
    Object.keys(formControls).forEach(name => {
      isFormValid = formControls[name].valid && isFormValid
    })
    this.setState({
      formControls, isFormValid
    })
  };

  createProductHandler = event => {
    console.log(this.state);
  };

  renderInputs () {
        return Object.keys(this.state.formControls).map((controlName, index) => {
        const control = this.state.formControls[controlName]
         
        return (         
          <TextField
          key={index}
          shouldvalidate={control.validation}
          valid={control.valid}
          touched={control.touched}
          label={controlName}
          margin="normal"
          defaultValue={control.value}
          onChange={event => this.handleChange(event, controlName)}
        />                
        )       
    })
  }

  render() {
    const { classes } = this.props;
    const {
      name,
      category,
      imageSrc,
      size,
      height,
      process,
      price,
      priceColor
    } = this.state.formControls;
    const values = {
      name,
      category,
      imageSrc,
      size,
      height,
      process,
      price,
      priceColor
    };

    return (
      <Grid>
        <Grid container justify="center" alignItems="center">
          <div className={classes.root}>
          
           {this.renderInputs()}
            

            <Button
              onClick={this.createProductHandler}
              variant="contained"
              color="primary"
              className={classes.button}
            >
              Create Product
            </Button>
          </div>
        </Grid>
      </Grid>
    );
  }
}
CreateProduct.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(CreateProduct);
