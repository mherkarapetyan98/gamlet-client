import React from "react";
import PropTypes from "prop-types";
import { Editor, EditorState, RichUtils } from "draft-js";
import { withStyles } from "@material-ui/core/styles";
import axios from "axios";
import Button from "@material-ui/core/Button";
import Icon from "@material-ui/core/Icon";
import IconClose from "@material-ui/icons/Close";
import Dialog from "@material-ui/core/Dialog";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import CloseIcon from "@material-ui/icons/Close";
import Slide from "@material-ui/core/Slide";
import classNames from "classnames";
import MenuItem from "@material-ui/core/MenuItem";
import TextField from "@material-ui/core/TextField";
import "./CreateService.css";

const styles = theme => ({
  appBar: {
    position: "relative"
  },
  flex: {
    flex: 1,
    flexDirection: "column"
  },
  container: {
    display: "flex",
    flexWrap: "wrap"
  },
  dense: {
    marginTop: 16
  },
  menu: {
    width: 390
  },
  input: {
    display: "none"
  }
});

function Transition(props) {
  return <Slide direction="up" {...props} />;
}

let imagesPreview = [];
let mainImage = [];
let moreImages = [];

class CreateService extends React.Component {
  state = {
    isFile: false,
    imagePrewiewUrl: "",
    moreImagesPreview: [],
    mainImage: null,
    moreImages: null,
    name: "",
    serviceDescription: "",
    multiline: "Controlled",
    currency: "EUR",
    editorState: EditorState.createEmpty()
  };

  handleChange = name => event => {
    this.setState({
      [name]: event.target.value
    });
  };

  uploadMainImageHandler = event => {
    event.preventDefault();
    console.log(event.target.files);
    const reader = new FileReader();
    const file = event.target.files[0];

    reader.addEventListener(
      "load",
      () => {
        this.setState({
          imagePrewiewUrl: reader.result,
          mainImage: file
        });
      },
      false
    );
    reader.readAsDataURL(file);
  };

  uploadMoreImagesHandler = event => {
    event.preventDefault();
    const file = event.target.files;
    // console.log(file);
    for (let i = 0; i < file.length; i++) {
      const reader = new FileReader();
      const image = file[i];
      reader.addEventListener(
        "load",
        () => {
          imagesPreview.push(reader.result);
          moreImages.push(image);
          // console.log(imagesPreview)
          this.setState({
            moreImagesPreview: imagesPreview,
            moreImages
            // icon: file
          });
        },
        false
      );
      reader.readAsDataURL(image);
    }
    // reader.addEventListener(
    //   "load",
    //   () => {
    //     this.setState({
    //       imagePrewiewUrl: reader.result,
    //       icon: file
    //     });
    //   },
    //   false
    // );
    // reader.readAsDataURL(file);
  };

  removeImagePreview = () => {
    this.setState({
      imagePrewiewUrl: false,
      icon: null
    });
  };

  renderMoreImages = () => {
    // console.log(this.state.moreImagesPreview)
    return Object.keys(this.state.moreImagesPreview).map(index => {
      const img = this.state.moreImagesPreview[index];
      return (
        <div
          style={{ display: "flex", width: "45%", padding: "15px" }}
          key={index}
        >
          <IconClose
            onClick={() => {
              // let previewImages = [...this.state.moreImagesPreview]
              // previewImages.splice(index, 1)
              imagesPreview.splice(index, 1);
              moreImages.splice(index, 1);
              this.setState({
                moreImagesPreview: imagesPreview,
                moreImages
              });
            }}
          />
          <img
            src={img}
            alt="service"
            key={index}
            style={{ width: "100%", height: "100%", padding: "10px" }}
          />
        </div>
      );
    });
  };

  componentDidMount() {
    // console.log(this.state)
  }

  createServiceHandler = async event => {
    event.preventDefault();
    const data = new FormData();
    const productData = {
      name: this.state.name,
      serviceDescription: this.state.serviceDescription
    };

    

    data.append("name", this.state.name);
    data.append("serviceDescription", this.state.serviceDescription);
    data.append("mainImage", this.state.mainImage, this.state.mainImage.name);
    this.state.moreImages.map(image => {
      data.append('moreImages', image, image.name)
    })
    const contentType = {
      headers: {
        "content-type": "multipart/form-data"
      }
    };

    try {
      const response = await axios.post('http://gamletstroy.ru/api/services/', data, contentType)
      console.log(response.data)
    } catch (error) {
      console.log(error);
    }
  };

  render() {
    const { classes } = this.props;
    return (
      <div>
        <Dialog
          fullScreen
          open={this.props.open}
          onClose={this.props.handleClose}
          TransitionComponent={Transition}
        >
          <AppBar className={classes.appBar}>
            <Toolbar>
              <IconButton
                color="inherit"
                onClick={this.props.handleClose}
                aria-label="Close"
              >
                <CloseIcon />
              </IconButton>
              <Typography variant="h6" color="inherit" className={classes.flex}>
                Создать сервис
              </Typography>
              <Button
                color="inherit"
                onClick={event => this.createServiceHandler(event)}
              >
                Сохранить
              </Button>
            </Toolbar>
          </AppBar>
          <div className="create-service__wrapper">
            <TextField
              id="filled-name"
              label="Имя сервиса"
              className={classes.textField}
              value={this.state.name}
              onChange={this.handleChange("name")}
              margin="normal"
              variant="filled"
            />
            <TextField
              placeholder="Описание сервиса"
              variant="filled"
              multiline={true}
              rows={5}
              onChange={this.handleChange("serviceDescription")}
              rowsMax={15}
              margin="normal"
            />
            {!!this.state.imagePrewiewUrl ? (
              <div className="service__main-image-preview">
                <IconClose
                  className="image-preview_close"
                  onClick={() => {
                    this.setState({ imagePrewiewUrl: null, mainImage: null });
                  }}
                />
                <img src={this.state.imagePrewiewUrl} alt="main" />
              </div>
            ) : (
              <div className="service__main-image_block">
                <input
                  accept="image/*"
                  className={classes.input}
                  id="contained-button-file"
                  type="file"
                  onChange={event => this.uploadMainImageHandler(event)}
                />
                <label htmlFor="contained-button-file">
                  <Button
                    variant="contained"
                    component="span"
                    className={classes.button}
                    fullWidth
                  >
                    Загрузить главную картину
                  </Button>
                </label>
              </div>
            )}

            <div className="service__more-images_block">
              <input
                accept="image/*"
                className={classes.input}
                id="contained-button__more-images"
                type="file"
                multiple
                onChange={event => this.uploadMoreImagesHandler(event)}
              />
              <label htmlFor="contained-button__more-images">
                <Button
                  variant="contained"
                  component="span"
                  className={classes.button}
                >
                  Загрузить дополнительные картинки
                </Button>
              </label>
            </div>
            <div className="service__more-images-previews">
              {this.renderMoreImages()}
            </div>
          </div>
        </Dialog>
      </div>
    );
  }
}

CreateService.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(CreateService);
