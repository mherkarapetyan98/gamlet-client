import React from "react";
import axios from "axios";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import { Link } from "react-router-dom";
import {
  Grid,
  Table,
  TableBody,
  TableCell,
  TableFooter,
  TablePagination,
  TableRow,
  Paper,
  IconButton,
  TableHead,
  Button
} from "@material-ui/core";
import FirstPageIcon from "@material-ui/icons/FirstPage";
import KeyboardArrowLeft from "@material-ui/icons/KeyboardArrowLeft";
import KeyboardArrowRight from "@material-ui/icons/KeyboardArrowRight";
import LastPageIcon from "@material-ui/icons/LastPage";
// import { reqURL } from "./../../../../../config/reqURL";
import {reqURL} from "../../../../../../config/reqURL"
const actionsStyles = theme => ({
  root: {
    flexShrink: 0,
    color: theme.palette.text.secondary,
    marginLeft: theme.spacing.unit * 2.5
  }
});

class TablePaginationActions extends React.Component {
  handleFirstPageButtonClick = event => {
    this.props.onChangePage(event, 0);
  };

  handleBackButtonClick = event => {
    this.props.onChangePage(event, this.props.page - 1);
  };

  handleNextButtonClick = event => {
    this.props.onChangePage(event, this.props.page + 1);
  };

  handleLastPageButtonClick = event => {
    this.props.onChangePage(
      event,
      Math.max(0, Math.ceil(this.props.count / this.props.rowsPerPage) - 1)
    );
  };

  handleChange = panel => (event, expanded) => {
    this.setState({
      expanded: expanded ? panel : false
    });
  };
  render() {
    const { classes, count, page, rowsPerPage, theme } = this.props;

    return (
      <div className={classes.root}>
        <IconButton
          onClick={this.handleFirstPageButtonClick}
          disabled={page === 0}
          aria-label="First Page"
        >
          {theme.direction === "rtl" ? <LastPageIcon /> : <FirstPageIcon />}
        </IconButton>
        <IconButton
          onClick={this.handleBackButtonClick}
          disabled={page === 0}
          aria-label="Previous Page"
        >
          {theme.direction === "rtl" ? (
            <KeyboardArrowRight />
          ) : (
            <KeyboardArrowLeft />
          )}
        </IconButton>
        <IconButton
          onClick={this.handleNextButtonClick}
          disabled={page >= Math.ceil(count / rowsPerPage) - 1}
          aria-label="Next Page"
        >
          {theme.direction === "rtl" ? (
            <KeyboardArrowLeft />
          ) : (
            <KeyboardArrowRight />
          )}
        </IconButton>
        <IconButton
          onClick={this.handleLastPageButtonClick}
          disabled={page >= Math.ceil(count / rowsPerPage) - 1}
          aria-label="Last Page"
        >
          {theme.direction === "rtl" ? <FirstPageIcon /> : <LastPageIcon />}
        </IconButton>
      </div>
    );
  }
}

TablePaginationActions.propTypes = {
  classes: PropTypes.object.isRequired,
  count: PropTypes.number.isRequired,
  onChangePage: PropTypes.func.isRequired,
  page: PropTypes.number.isRequired,
  rowsPerPage: PropTypes.number.isRequired,
  theme: PropTypes.object.isRequired
};

const TablePaginationActionsWrapped = withStyles(actionsStyles, {
  withTheme: true
})(TablePaginationActions);

const styles = theme => ({
  root: {
    width: "100%",
    marginTop: theme.spacing.unit * 3
  },
  table: {
    minWidth: 500
  },
  tableWrapper: {
    overflowX: "auto"
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    flexShrink: 0
  },
  secondaryHeading: {
    fontSize: theme.typography.pxToRem(15),
    color: theme.palette.text.secondary
  }
});

class ServiceList extends React.Component {
  state = {
    rows: [],
    header: [
      {
        name: "Name",
        prop: "name"
      },
      {
        name: "Size",
        prop: "size"
      },
      {
        name: "Price",
        prop: "price"
      }
    ],
    page: 0,
    rowsPerPage: 10
  };

  handleChangePage = (event, page) => {
    this.setState({ page });
  };

  handleChangeRowsPerPage = event => {
    this.setState({ rowsPerPage: event.target.value });
  };

  async componentDidMount() {
    try {
      const response = await axios.get(
        `${reqURL.getTrotuarAdmin}${this.props.categoryKey}`
      );
      const rows = this.state.rows.concat();
      Object.keys(response.data).map(index => {
        rows.push(response.data[index]);
      });
      this.setState({
        rows
      });
    } catch (error) {
      console.log(error);
    }
  }

  render() {
    const { classes } = this.props;
    const { rows, rowsPerPage, page, header } = this.state;
    const emptyRows =
      rowsPerPage - Math.min(rowsPerPage, rows.length - page * rowsPerPage);
    return (
      <div>
        list
      </div>
      // <Grid item xs={12}>
      //   <Paper className={classes.root}>
      //     <Table className={classes.table}>
      //       <TableHead>
      //         <TableRow>
      //           {header.map((x, i) => (
      //             <TableCell key={`thc-${i}`}>{x.name}</TableCell>
      //           ))}
      //         </TableRow>
      //       </TableHead>
      //       <TableBody>
      //         {rows
      //           .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
      //           .map((row, index) => {
      //             return (
      //               <TableRow key={row._id}>
      //                 <TableCell>{row.name}</TableCell>
      //                 <TableCell>{row.size}</TableCell>
      //                 <TableCell>{row.price}</TableCell>
      //                 <TableCell>
      //                   <Button
      //                     variant="contained"
      //                     className={classes.button}
      //                     size="small"
      //                     component={Link}
      //                     to={`${this.props.history.location.pathname}/${
      //                       row._id
      //                     }`}
      //                   >
      //                     Изменить
      //                   </Button>
      //                 </TableCell>
      //               </TableRow>
      //             );
      //           })}
      //         {emptyRows > 0 && (
      //           <TableRow style={{ height: 48 * emptyRows }}>
      //             <TableCell colSpan={6} />
      //           </TableRow>
      //         )}
      //       </TableBody>
      //       <TableFooter>
      //         <TableRow>
      //           <TablePagination
      //             rowsPerPageOptions={[5, 10, 25]}
      //             colSpan={3}
      //             count={rows.length}
      //             rowsPerPage={rowsPerPage}
      //             page={page}
      //             onChangePage={this.handleChangePage}
      //             onChangeRowsPerPage={this.handleChangeRowsPerPage}
      //             ActionsComponent={TablePaginationActionsWrapped}
      //           />
      //         </TableRow>
      //       </TableFooter>
      //     </Table>
      //   </Paper>
      // </Grid>
    );
  }
}

ServiceList.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(ServiceList);
