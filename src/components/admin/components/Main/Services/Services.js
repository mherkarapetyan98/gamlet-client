import React, { Component } from "react";
import ServiceList from "./ServiceList/ServiceList";
import AddIcon from "@material-ui/icons/Add";
import CreateService from "./CreateService/CreateService";

export default class Services extends Component {
  state = {
    open: false
  };

  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  render() {
    return (
      <div>
        <div className="add-service">
          <AddIcon
            style={{ cursor: "pointer" }}
            fontSize="large"
            titleAccess="Добавить сервис"
            onClick={this.handleClickOpen}
          />
          {this.state.open ? <CreateService  open={this.state.open} handleClose={this.handleClose}/> : null}
        </div>
        <div className="service-list">
          <ServiceList />
        </div>
      </div>
    );
  }
}
