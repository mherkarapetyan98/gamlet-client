import SnackbarComponent from "../../../../UI/Snackbar/Snackbar";
import React, { Component } from "react";
import {
  Grid,
  InputAdornment,
  TextField,
  Button,
  MenuItem
} from "@material-ui/core";
import classNames from "classnames";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import axios from "axios";
import { reqURL } from "./../../../../../config/reqURL";

window.__MUI_USE_NEXT_TYPOGRAPHY_VARIANTS__ = true;

const styles = theme => ({
  root: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    flexWrap: "wrap"
  },
  margin: {
    margin: theme.spacing.unit
  },
  input: {
    display: "none"
  }
});

const methods = [
  {
    value: "Вибролитье",
    label: "Вибролитье"
  },
  {
    value: "Вибропресс",
    label: "Вибропресс"
  }
];

class Edit extends Component {
  state = {
    adminChange: false,
    isFormValid: false,
    product: "",
    formControls: {
      name: {
        value: "",
        type: "text",
        valid: false,
        validation: {
          required: true
        }
      },
      date: {
        value: "",
        type: "text",
        valid: false,
        validation: {
          required: true
        }
      },
      review: {
        value: "",
        type: "text",
        valid: false,
        validation: {
          required: true
        }
      }
    }
  };

  validateControl(value, validation) {
    if (!validation) {
      return true;
    }

    let isValid = true;
    if (validation.required) {
      isValid = value.trim() !== "" && isValid;
    }

    return isValid;
  }

  handleChange = (event, controlName) => {
    const formControls = { ...this.state.formControls };
    const control = { ...formControls[controlName] };

    control.value = event.target.value;

    control.touched = true;
    control.valid = this.validateControl(control.value, control.validation);

    formControls[controlName] = control;
    let isFormValid = true;
    Object.keys(formControls).forEach(name => {
      isFormValid = formControls[name].valid && isFormValid;
    });
    this.setState({
      formControls,
      isFormValid
    });
  };

  uploadImage = prop => event => {
    this.setState({ [prop]: event.target.value });
  };

  updateProductHandler = async event => {
    const data = new FormData();
    const productData = {
      name: this.state.formControls.name.value,
      date: this.state.formControls.date.value,
      createdDate: this.state.formControls.date.value,
      review: this.state.formControls.review.value
    };
 
     try {
      await axios.patch(
        `http://gamletstroy.ru/api/reviews/${this.props.reviewId}`,
        productData, 
      );
      this.setState({ adminChange: true });
      setTimeout(() => {
        this.setState({ adminChange: false });
      }, 2000);
    } catch (error) {
      console.log(error);
    }
  };

  renderInputs() {
    return Object.keys(this.state.formControls).map((controlName, index) => {
      const control = this.state.formControls[controlName];
      return (
        <TextField
          required
          key={index}
          type={control.type}
          value={control.value}
          shouldvalidate={`${!!control.validation}`}
          isvalid={`${control.valid}`}
          touched={`${control.touched}`}
          label={controlName}
          margin="normal"
          onChange={event => this.handleChange(event, controlName)}
        />
      );
    });
  }

  getProductHandler = async () => {
    try {
      const url = `http://gamletstroy.ru/api/reviews/${this.props.reviewId}`;
      const response = await axios.get(url);
      const product = response.data; 
      this.setState({
        priceColor: product.priceColor,
        process: product.process,
        formControls: {
          name: { value: product.name },
          date: { value: product.date },
          review: { value: product.review }
        }
      });
    } catch (error) {
      console.log(error);
    }
  };

  componentDidMount() {
    this.getProductHandler();
  }

  render() {
    const { classes } = this.props;

    return (
      <Grid>
        {this.renderInputs()}
        <Button
          onClick={this.updateProductHandler}
          variant="contained"
          color="primary"
          className={classes.button}
        >
          Update
        </Button>
      </Grid>
    );
  }
}
Edit.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(Edit);
