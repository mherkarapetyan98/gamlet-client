import SnackbarComponent from  '../../../../UI/Snackbar/Snackbar'
import React, { Component } from "react";
import { Grid, InputAdornment, TextField, Button , MenuItem} from "@material-ui/core";
import classNames from "classnames";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import axios from 'axios';
import { reqURL } from './../../../../../config/reqURL'; 

window.__MUI_USE_NEXT_TYPOGRAPHY_VARIANTS__ = true;

const styles = theme => ({  
  root: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    flexWrap: "wrap"
  },
  margin: {
    margin: theme.spacing.unit
  },
  input: {
    display: "none"
  }
});

const methods = [
  {
    value: 'Вибролитье',
    label: 'Вибролитье',
  },
  {
    value: 'Вибропресс',
    label: 'Вибропресс',
  }
]
 
class Edit extends Component {
  state = {
    adminChange: false,
    isFormValid: false,   
    categories: [], 
    product: '',
    category:  '',
    imageSrc:  null,
    priceColor: {
      value: "",
      valid: false, 
      validation: {
        required: false
      }
    },
    process: '',
    formControls: {
      name: {
        value: "",
        type: "text",
        valid: false,
        validation: {
          required: true
        }
      },
      size: {
        value: "",
        type: "text",
        valid: false,
        validation: {
          required: true
        }
      },
      height: {
        value: "",
        type: "text",
        valid: false,
        validation: {
          required: true
        }
      },
      quantity: {
        value: "",
        type: "text",
        valid: false,
        validation: {
          required: false
        }
      },
      price: {
        value: "",
        type: "number",
        valid: false,
        validation: {
          required: true
        }
      },
      priceColor: {
        value: "",
        type: "number",
        valid: true
      },

    }
  
  };

  validateControl(value, validation) {
    if(!validation) {
      return true
    }

    let isValid = true;
    if(validation.required) {
      isValid = value.trim() !== '' && isValid
    }
   

    return isValid;
  }

  handleChange = (event, controlName) => {
    const formControls = {...this.state.formControls};
    const control = {...formControls[controlName]};
    
    control.value = event.target.value;

    control.touched = true;
    control.valid = this.validateControl(control.value, control.validation);

    formControls[controlName] = control ;
    let isFormValid = true;
    Object.keys(formControls).forEach(name => {
      isFormValid = formControls[name].valid && isFormValid
    })
    this.setState({
      formControls, isFormValid
    })
  };

  uploadImage = prop => event => {
    this.setState({ [prop]: event.target.value });
  };


  updateProductHandler = async event => {
    const data = new FormData();
     const productData = {
      name:  this.state.formControls.name.value,
      category: this.state.category, 
      size: this.state.formControls.size.value,
      height: this.state.formControls.height.value,
      quantity: this.state.formControls.quantity.value,
      process: this.state.process,
      price: this.state.formControls.price.value,
      priceColor: this.state.formControls.priceColor.value
     }

     data.append('productData', productData);
     data.append('name', this.state.formControls.name.value)
     data.append('category', this.state.category)
     data.append('size', this.state.formControls.size.value)
     data.append('height', this.state.formControls.height.value)
     data.append('quantity', this.state.formControls.quantity.value)
     data.append('process', this.state.process)
     data.append('price', this.state.formControls.price.value)
     data.append('priceColor', this.state.formControls.priceColor.value)
     data.append('imageName', this.state.imageSrc ? this.state.imageSrc.name : "")
 
     data.append('image', this.state.imageSrc ? this.state.imageSrc : null, this.state.imageSrc ? this.state.imageSrc.name : null)
    //  data.append('imageName', this.state.imageSrc.name)

     const url = `${reqURL.createProduct}${this.props.match.params.id}`
     console.log(this.state)
     const contentType = {
       headers: {
         "content-type": "multipart/form-data"
       }
     }
     try {
       console.log(this.state.imageSrc)
       await axios.patch(`${reqURL.createProduct}${this.props.match.params.id}` ,  data, contentType)  
       this.setState({adminChange: true})
       setTimeout( () => {this.setState({adminChange: false})}, 2000)       
    } catch (error) {
      console.log(error)
    }

  };

  deleteProductHandler = async () => { 
      const url = `http://gamletstroy.ru/api/admin/${this.props.match.params.id}`
      try {
          const response = await axios.delete(url)
          this.setState({adminChange: true})
          setTimeout( () => {this.props.history.goBack()}, 600)          
      } catch (error) {
          console.log(error)
      }
  }

  renderInputs () {
        return Object.keys(this.state.formControls).map((controlName, index) => {
        const control = this.state.formControls[controlName] 
        return (         
          <TextField
          required
          key={index}
          type={control.type}
          value={control.value}
          shouldvalidate={`${!!control.validation}`}
          isvalid={`${control.valid}`}
          touched={`${control.touched}`}
          label={controlName}
          margin="normal"          
          onChange={event => this.handleChange(event, controlName)}
        />                
        )       
    })
  }

  fileSelectedHandler = event => {
   this.setState({
       imageSrc: event.target.files[0]
    })
  }

  getCategoriesHandler = async () => {
    try {
        const response = await axios.get(reqURL.getCategory)
        const categories = response.data; 
        this.setState({
          categories
        })        
      } catch (error) {
        console.log(error)
      }
  }
 
  getProductHandler = async () => {
    try {
        // const url = `${reqURL.getProduct}${this.props.match.params.id}`
        const url = `${reqURL.getTrotuar}${this.props.categoryKey}/${this.props.match.params.id}`
        const response = await axios.get(url);
        const product = response.data;
        this.setState({
            category: product.category,
            priceColor: product.priceColor,
            process: product.process,
            formControls: {
                name: {value: product.name},        
                size: {value: product.size},
                height: {value: product.height},
                quantity: {value: product.quantity},
                price: {value: product.price},
                priceColor: {value: product.priceColor}
            } 
        })
      } catch (error) {
        console.log(error)
      }
  }


   componentDidMount() { 
      this.getCategoriesHandler();  
      this.getProductHandler();
  }
   
  render() {
    const { classes } = this.props;
    const {
      name,  
      size,
      height,
      process,
      price,
      priceColor
    } = this.state.formControls;
    const values = {
      name, 
      size,
      height,
      process,
      price,
      priceColor
    };
    return (
      <Grid>
        <Grid container justify="center" alignItems="center">
          <div className={classes.root}>
          
           {this.renderInputs()}

             <input
              accept="image/*"
              className={classes.input}
              id="contained-button-file"
              type="file"
               onChange={this.fileSelectedHandler}
            />
            <label htmlFor="contained-button-file">
              <Button
                variant="contained"
                component="span"
                className={classes.button}
                fullWidth                 
              >
                Upload image
              </Button>
            </label>


          <TextField
            select 
            className={classNames(classes.margin, classes.textField)}
            value={this.state.category}
            onChange={this.uploadImage('category')} 
            InputProps={{
              startAdornment: <InputAdornment position="start">Category</InputAdornment>,
            }}
          >
          {this.state.categories.map(option => (
            <MenuItem key={option._id} value={option._id}>
              {option.name}
            </MenuItem>
          ))}
        </TextField>
        <TextField
            select 
            className={classNames(classes.margin, classes.textField)}
            value={this.state.process}
            onChange={this.uploadImage('process')} 
            InputProps={{
              startAdornment: <InputAdornment position="start">process</InputAdornment>,
            }}
         >
          {methods.map(option => (
            <MenuItem key={option.value} value={option.value}>
              {option.label}
            </MenuItem>
          ))}
        </TextField>
            <Button
              onClick={this.updateProductHandler}
              variant="contained"
              color="primary"
              className={classes.button}
            >
              Update
            </Button>            
            <Button
              onClick={this.deleteProductHandler}
              variant="contained"
              color="secondary"
              className={classes.button}
            >
              Delete
            </Button>
          </div>
        </Grid>
        { this.state.adminChange 
         ? <SnackbarComponent 
            open={this.state.adminChange} 
             message={`Product ${this.state.formControls.name.value} deleted`}/> 
         : null}
        { this.state.adminChange 
         ? <SnackbarComponent 
            open={this.state.adminChange} 
            message={`Product ${this.state.formControls.name.value} updated`}/> 
         : null}

      </Grid>
    );
  }
}
Edit.propTypes = {
  classes: PropTypes.object.isRequired
};


export default withStyles(styles)(Edit);
