import React from 'react';
import axios from 'axios';
import { Route,  Switch, Link } from 'react-router-dom'
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab'; 
import Trotuar from './Trotuar';  
import Edit from './Edit';
import {reqURL} from '../../../../../config/reqURL';
// import Trotuar from './../../../../../containers/Products/content/trotuar/Trotuar';

const styles = {
  root: {
    flexGrow: 1,
    flexDirection: 'column'
  }
};

class ProductsList extends React.Component {
  state = {
    value: 0,
    categories: [],
  };

  handleChange = (event, value) => {
    this.setState({ value });
  };

  async componentDidMount() {
    try {
      const response = await axios.get(reqURL.getCategory)
      const categories = response.data;
      this.setState({
        categories
      })
    } catch (error) {
      console.log(error)
    }
  }

  render() {
    const { classes } = this.props;
    const { categories } = this.state;   
    return (
      <Paper className={classes.root}>
        <Tabs
          value={this.state.value}
          onChange={this.handleChange}
          indicatorColor="primary"
          textColor="primary"      
           variant="fullWidth"
        >
          {categories.map(category => {
            return (
              <Tab label={category.name} key={category._id} component={Link}  to={`/admin/products/${category.key}`}>
              
              </Tab>
             )
          })}
        </Tabs>
      
        {categories.map(category => {
            return (
              <Switch key={category.key}>
                <Route path={`/admin/products/${category.key}`} exact key={category.key}  render={(props) => <Trotuar {...props} categoryKey={category.key}  key={category.key} /> }/>
                  {/* // <Route path="/admin/products/trotuar" component={Trotuar} exact />  */}
                 <Route path={`/admin/products/${category.key}/:id`}  render={(props) => <Edit {...props} categoryKey={category.key}  key={category.key} /> } />    
                </Switch>                 
             )             
          })}
          
      </Paper>
    );
  }
}

ProductsList.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ProductsList);