import React from "react";
import axios from "axios";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import {
  Grid,
  Table,
  TableBody,
  TableCell,
  TableFooter,
  TablePagination,
  TableRow,
  Paper,
  IconButton,
  Button,
  ExpansionPanel, ExpansionPanelDetails, ExpansionPanelSummary
} from "@material-ui/core";
import FirstPageIcon from "@material-ui/icons/FirstPage";
import KeyboardArrowLeft from "@material-ui/icons/KeyboardArrowLeft";
import KeyboardArrowRight from "@material-ui/icons/KeyboardArrowRight";
import LastPageIcon from "@material-ui/icons/LastPage";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import Typography from '@material-ui/core/Typography';
import { reqURL } from './../../../../../config/reqURL';
import SnackbarComponent from "../../../../UI/Snackbar/Snackbar";

const actionsStyles = theme => ({
  root: {
    flexShrink: 0,
    color: theme.palette.text.secondary,
    marginLeft: theme.spacing.unit * 2.5
  }
});

class TablePaginationActions extends React.Component {
  handleFirstPageButtonClick = event => {
    this.props.onChangePage(event, 0);
  };

  handleBackButtonClick = event => {
    this.props.onChangePage(event, this.props.page - 1);
  };

  handleNextButtonClick = event => {
    this.props.onChangePage(event, this.props.page + 1);
  };

  handleLastPageButtonClick = event => {
    this.props.onChangePage(
      event,
      Math.max(0, Math.ceil(this.props.count / this.props.rowsPerPage) - 1)
    );
  };

  handleChange = panel => (event, expanded) => {
    this.setState({
      expanded: expanded ? panel : false
    });
  };
  render() {
    const { classes, count, page, rowsPerPage, theme } = this.props;

    return (
      <div className={classes.root}>
        <IconButton
          onClick={this.handleFirstPageButtonClick}
          disabled={page === 0}
          aria-label="First Page"
        >
          {theme.direction === "rtl" ? <LastPageIcon /> : <FirstPageIcon />}
        </IconButton>
        <IconButton
          onClick={this.handleBackButtonClick}
          disabled={page === 0}
          aria-label="Previous Page"
        >
          {theme.direction === "rtl" ? (
            <KeyboardArrowRight />
          ) : (
              <KeyboardArrowLeft />
            )}
        </IconButton>
        <IconButton
          onClick={this.handleNextButtonClick}
          disabled={page >= Math.ceil(count / rowsPerPage) - 1}
          aria-label="Next Page"
        >
          {theme.direction === "rtl" ? (
            <KeyboardArrowLeft />
          ) : (
              <KeyboardArrowRight />
            )}
        </IconButton>
        <IconButton
          onClick={this.handleLastPageButtonClick}
          disabled={page >= Math.ceil(count / rowsPerPage) - 1}
          aria-label="Last Page"
        >
          {theme.direction === "rtl" ? <FirstPageIcon /> : <LastPageIcon />}
        </IconButton>
      </div>
    );
  }
}

TablePaginationActions.propTypes = {
  classes: PropTypes.object.isRequired,
  count: PropTypes.number.isRequired,
  onChangePage: PropTypes.func.isRequired,
  page: PropTypes.number.isRequired,
  rowsPerPage: PropTypes.number.isRequired,
  theme: PropTypes.object.isRequired
};

const TablePaginationActionsWrapped = withStyles(actionsStyles, {
  withTheme: true
})(TablePaginationActions);


const styles = theme => ({
  root: {
    width: "80%",
    margin: '0 auto',
    marginTop: theme.spacing.unit * 3
  },
  table: {
    minWidth: 500,
    width: "100%",
    flexDirection: 'column',

  },
  tableCell: {
    alignItems: 'center'
  },
  tableWrapper: {
    overflowX: "auto"
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    flexShrink: 0
  },
  secondaryHeading: {
    fontSize: theme.typography.pxToRem(15),
    color: theme.palette.text.secondary
  },
  footing: {
    width: '100%'
  }
});


class Messages extends React.Component {
  state = {
    rows: [],
    expanded: null,
    page: 0,
    rowsPerPage: 5,
    deleted: false
  };

  handleChange = panel => (event, expanded) => {
    this.setState({
      expanded: expanded ? panel : false
    });
  };

  handleChangePage = (event, page) => {
    this.setState({ page });
  };

  handleChangeRowsPerPage = event => {
    this.setState({ rowsPerPage: event.target.value });
  };


  async componentDidMount() {
    try {
      const response = await axios.get(reqURL.getMessages)
      const rows = response.data;
      this.setState({
        rows
      })
    } catch (error) {
      console.log(error)
    }
  }


  deleteMessageHandler = async (event, id) => {
    event.preventDefault() 
    const url = `${reqURL.deleteMessage}${id}`
    try {
      const response = await axios.delete(url)
      await this.setState({
        rows: response.data,
        deleted: true
      })
      setTimeout(() => {
        this.setState({ deleted: false });
      }, 1000)
    } catch (error) {
      console.log(error)
    }
  }

  render() {
    const { classes } = this.props;
    const { expanded } = this.state;
    const { rows, rowsPerPage, page } = this.state;
    const emptyRows =
      rowsPerPage - Math.min(rowsPerPage, rows.length - page * rowsPerPage);

    return (
      <Grid item xs={12}>
        <Paper className={classes.root}>
          <Table className={classes.table} padding="checkbox">

            <TableBody>
              {rows
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((row, index) => {
                  row.open = false
                  return (
                    <TableRow key={row._id}  >
                      <TableCell  >
                        <ExpansionPanel
                          expanded={expanded === `${row._id}`}
                          onChange={this.handleChange(`${row._id}`)} 
                        >
                          <ExpansionPanelSummary
                            expandIcon={<ExpandMoreIcon />}
                          >
                            <Typography className={classes.heading} component={'div'} variant={'body2'}>
                              <TableCell component="div" scope="row">{row.name}</TableCell>
                              <TableCell component="div" numeric>{row.phone}</TableCell>
                              <TableCell component="div" numeric>{row.date}</TableCell>
                            </Typography>
                          </ExpansionPanelSummary>

                          <ExpansionPanelDetails>
                            <Typography component={'span'} variant={'body2'}>{row.message} <br /></Typography>

                            <Button
                              onClick={(event) => this.deleteMessageHandler(event, row._id)}
                              variant="contained"
                              color="secondary"
                              className={classes.button}
                            >
                              Удалить
                           </Button>
                          </ExpansionPanelDetails>
                        </ExpansionPanel>
                      </TableCell>

                    </TableRow>
                  );
                })}
              {emptyRows > 0 && (
                <TableRow style={{ height: 48 * emptyRows }}>
                  <TableCell colSpan={6} />
                </TableRow>
              )}
            </TableBody>
            <TableFooter className={classes.footing}>
              <TableRow>
                <TablePagination
                  rowsPerPageOptions={[5, 10, 25]}
                  colSpan={3}
                  count={rows.length}
                  rowsPerPage={rowsPerPage}
                  page={page}
                  onChangePage={this.handleChangePage}
                  onChangeRowsPerPage={this.handleChangeRowsPerPage}
                  ActionsComponent={TablePaginationActionsWrapped}
                />
              </TableRow>
            </TableFooter>
          </Table>
        </Paper>
        {this.state.deleted ? <SnackbarComponent open={this.state.deleted} message="Message deleted" /> : null}
      </Grid>
    );
  }
}

Messages.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(Messages);