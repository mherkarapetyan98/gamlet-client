import React, { Component } from 'react';
import "./CallModal.css";
import ContactUs from '../ContactUs/ContactUs';

class CallModal extends Component {
  render() {
    return (
      <div className="CallModal">
            <ContactUs modalContact={true} closeModalContact={this.props.closeModalContact}/> 
      </div>
    )
  }
}

export default CallModal;