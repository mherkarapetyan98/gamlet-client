import { YMaps, Map, Placemark } from "react-yandex-maps";
import React from "react";
import './Map.css'

export default function MapComponent()   {
  return (
    <div className="main-map" >
      <h2>Мы здесь</h2>
      <div className="main-map__map">
        <YMaps>
          <Map
            instanceRef={ref => {
              ref && ref.behaviors.disable("scrollZoom");
            }}
            width="100%"
            height="400px"
            defaultState={{
              center: [55.436407, 37.792375],
              zoom: 10
            }}
          >
            {" "}
            <Placemark geometry={[55.436407, 37.592375]   } />
            <Placemark geometry={[55.372788851626154,38.04684388891596]} />
          </Map>
        </YMaps>
      </div>
    </div>
  );
};
