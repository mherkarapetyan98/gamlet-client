import React, { Component } from "react";
import Slider from "react-slick";
// import {OrderButton} from "../UI/Button/Button";
import "./MainSlider.css";

function SampleNextArrow(props) {
  const { className, onClick } = props;
  // return <div className={`${className} arrow`} onClick={onClick} />;
  return <div className="slider-arrow-end prev-arrow"><i className="fas fa-less-than" onClick={onClick}/></div>
}

function SamplePrevArrow(props) {
  const { className, onClick } = props;
  // return <div className={`${className}  arrow`} onClick={onClick} />;
  return <div className="slider-arrow-end next-arrow"><i className="fas fa-greater-than" onClick={onClick} /></div>
}
export default class mainSlider extends Component {
  state = {
    images: [
      
      {
        src: require("../../img/slider/4.jpg"),
        alt: "Hio",
        text: "ХУДОЖЕСТВЕННАЯ КОВКА",
        // discount: "Скидки до 15%"
      },
      {
        src: require("../../img/slider/1.jpg"),
        alt: "Тротуарная плитка",
        text: "При покупке тротуарной плитки более 100 м² ",
        discount: "получите 5% скидку"
      },
      {
        src: require("../../img/slider/5.png"),
        alt: "Hio",
        text: "До 10км доставка",
        discount: "бесплатно"
      },
      {
        src: require("../../img/slider/2.jpg"),
        alt: "Hio",
        text: "ВЫГОДНАЯ АРЕНДА СПЕЦТЕХНИКИ",
        // discount: "Скидки до 15%"
      },
      {
        src: require("../../img/slider/3.jpg"),
        alt: "Hio",
        text: "АСФАЛЬТИРОВАНИЕ ДОРОГ",
        // discount: "Скидки до 15%"
      }
    ]
  };
  renderImages() {
    return Object.keys(this.state.images).map((img, index) => {
      const image = this.state.images[img];
      return (
        <div className="overlay" key={index}>
          <img src={image.src} alt={image.alt} />
          <div className="slider-text">
           <h1>{image.text}</h1>              
            <h1 className="discount"> {image.discount ? image.discount : null } </h1>         
             {/* <OrderButton /> */}
          </div>
          
        </div>
      );
    });
  }

  render() {
    const settings = {
      infinite: true,
      speed: 1000,
      autoplay: true,
      autoplaySpeed: 8500,
      arrows: true,
      slidesToScroll: 1,
      slidesToShow: 1,
      pauseOnFocus: false,
      pauseOnHover: false,
      adaptiveHeight: true,
      nextArrow: <SampleNextArrow />,
      prevArrow: <SamplePrevArrow />
    };
    return (
      <div className="MainSlider">
        <Slider {...settings}>
          {this.renderImages()}  
              
        </Slider>
  
      </div>
    );
  }
}
