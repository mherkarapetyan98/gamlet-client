import React from "react";
import "./Input.css";

const Input = props => {
  const inputType = props.type || "text";

  return (
    <div className="Input" onClick={props.onClick}>
      <label>
        {props.label}
        <input
          type={inputType}
          value={props.value}
          placeholder={props.placeholder}
          onChange={props.onChange}
        />
      </label>
    </div>
  );
};

export default Input;
