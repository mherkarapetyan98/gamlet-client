import React from 'react';
import { NavLink } from "react-router-dom";
import './Button.css';


export  function OrderButton ()  {
  return (
    <div className="Button order-button">
        <NavLink to="/contact">
            Заказать
        </NavLink>    
    </div>
  )
}

export  function ServicesButton ()  {
  return (
    <div className="Button">
        <NavLink to="/services">
            Посмотреть услуги
        </NavLink>    
    </div>
  )
}
 
 