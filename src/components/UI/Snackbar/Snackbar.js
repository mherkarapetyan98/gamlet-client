import React from 'react';
import Snackbar from '@material-ui/core/Snackbar';

class SnackbarComponent extends React.Component {
  state = {
    open: !this.props.deleted,
    vertical: 'bottom',
    horizontal: 'rigth',
  };

  handleClick = state => () => {
    this.setState({ open: true, ...state });
  };

  handleClose = () => {
    this.setState({ open: false})
  };

  render() {
    // const { vertical, horizontal, open } = this.state;
    return (
      <div>
        <Snackbar
          anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
          open={this.props.open}
          ContentProps={{
            'aria-describedby': 'message-id',
          }}
          message={<span id="message-id">{this.props.message}</span>}
        />
      </div>
    );
  }
}

export default SnackbarComponent;