import React from 'react';
import './Headline.css'

export default (props) => {
  return (
    <div className="Headline">
      {props.title}
    </div>
  )
}
