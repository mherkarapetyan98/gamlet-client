// import React, { Component } from "react";
// import Slider from "react-slick";
// // import { baseUrl } from "./config";
// import "./ServicesSlider.css";

// export default class ServicesSlider extends Component {
//   state = {
//     images: []
//   };
//   renderImages() {
//     return Object.keys(this.state.images).map((img, index) => {
//       const image = this.state.images[img];
//       return (
//         <div className="overlay" key={index}>
//           <img src={image.src} alt={image.alt} />
//         </div>
//       );
//     });
//   }

//   // componentDidMount() {
//   //   // this.props.moreImages.map(image => console.log(image.src));
//   //   console.log(this.props.moreImages)
//   //   this.setState({
//   //     imamges: this.props.moreImages
//   //   })
//   // }

//   renderImagesHandler = () => {
//     return Object.keys(this.props.moreImages).map(index => {
//       const image = this.props.moreImages[index];
//       return (
//         <div key={index}>
//           {/* <img src={`http://localhost:3000${image.src}`} alt={image.alt} /> */}
//           <h3>{`HI ${index}`}</h3>
//         </div>
//       );
//     });
//   };

//   render() {
//     const settings = {
//       infinite: true,
//       slidesToShow: 3,
//       slidesToScroll: 1,
//       autoplay: true,
//       speed: 2000,
//       autoplaySpeed: 2000,
//       cssEase: "linear"
//     };
//     console.log(this.props.moreImages);
//     return (
//       <div>
//         {this.props.moreImages ? (
//           <Slider {...settings}>

//             {/* <div className="service-slider">{this.renderImagesHandler()}</div> */}
//             <div>
//               {/* <img src={`http://localhost:3000${image.src}`} alt={image.alt} /> */}
//               <h3>{`HI 1`}</h3>
//             </div>
//             <div>
//               <h3>
//                 Hi 2
//               </h3>
//             </div>
//           </Slider>
//         ) : null}
//       </div>
//     );

//     // return (
//     //   <div>

//     //     <Slider {...settings}>
//     //       <div>
//     //         <div>
//     //           <img src="http://localhost:3000/static/media/4.4fdf5bca.jpg" />
//     //         </div>
//     //         <div>
//     //           <img src="http://localhost:3000/static/media/6.6102b8c4.jpg" />
//     //         </div>
//     //       </div>
//     //     </Slider>
//     //   </div>
//     // );
//   }
// }
import React, { Component } from "react";
import Slider from "react-slick";
import "./ServicesSlider.css";
import { Carousel } from "react-responsive-carousel";

let selected = ''

export default class ServicesSlider extends Component {
  renderImages() {
    return Object.keys(this.props.moreImages).map((img, index) => {
      const image = this.props.moreImages[img]; 
      if(this.props.selectedImage.alt === image.alt && !!this.props.selectedImage) {
        selected = img 
      }
      return (
        <div key={index} className="service-slider">
          <img src={image.src} alt={image.alt} />
        </div>
      );
    });
  }
  render() {  
    return (
      // <div>
      //   <Slider {...settings}>{this.renderImages()}</Slider>
      // </div>
      <Carousel
        selectedItem={0}
        autoPlay={true}
        emulateTouch
        centerSlidePercentage={65}
        centerMode={true}
        infiniteLoop
        width="100%"
        className="services-slider"
      >
        {this.renderImages()}
      </Carousel>
    );
  }
}
