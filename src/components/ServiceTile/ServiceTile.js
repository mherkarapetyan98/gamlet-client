import React from 'react'
import  './ServiceTile.css'

export default (props) => {
  return (
    <div className="service-tile">
            <img src={props.image} alt={props.title}/>
            <span>{props.title}</span>
    </div>
  )
}
