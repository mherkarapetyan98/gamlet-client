import React, { Component } from "react";
import "./ContactUs.css";
import axios from "axios";
import moment from "moment";
import SnackbarComponent from "../UI/Snackbar/Snackbar";
import { reqURL } from "../../config/reqURL";

export default class ContactUs extends Component {
  constructor(props) {
    super(props);
    this._initState = {
      isSended: false,
      isFormValid: false,
      formControls: {
        name: {
          value: "",
          label: "Ваше имя*",
          type: "text",
          valid: false,
          touched: false,
          errorMessage: "Գրեք ճիշտ անուն",
          validation: {
            required: true,
            minLength: 3,
            maxLength: 32
          }
        },
        phone: {
          value: "",
          label: "Ваш телефон*",
          type: "text",
          valid: false,
          touched: false,
          errorMessage: "Գրեք ճիշտ անուն",
          validation: {
            required: true,
            minLength: 4,
            maxLength: 32
          }
        }
      },
      message: {
        value: "",
        label: "Ваши комментарии",
        type: "text",
        valid: false,
        touched: false,
        errorMessage: "Գրեք ճիշտ անուն",
        validation: {
          minLength: 4,
          maxLength: 32
        }
      }
    };

    this.state = this._initState;
  }

  validateControl(value, validation) {
    if (!validation) {
      return true;
    }

    let isValid = true;
    if (validation.required) {
      isValid = value.trim() !== "" && isValid;
    }

    if (validation.minLength) {
      isValid = value.length >= validation.minLength && isValid;
    }

    if (validation.maxLength) {
      isValid = value.length <= validation.maxLength && isValid;
    }

    return isValid;
  }

  onChangeHandler = (event, controlName) => {
    const formControls = { ...this.state.formControls };
    const control = { ...formControls[controlName] };

    control.value = event.target.value;
    control.touched = true;
    control.valid = this.validateControl(control.value, control.validation);

    formControls[controlName] = control;

    let isFormValid = true;
    Object.keys(formControls).forEach(name => {
      isFormValid = formControls[name].valid && isFormValid;
    });

    this.setState({
      formControls,
      isFormValid
    });
  };

  /** textarea onchange handler */
  textareaHandler = (event, controlName) => {
    event.preventDefault();
    const message = { ...this.state.message };
    message.value = event.target.value;
    this.setState({
      message
    });
  };
  sendMessageHandler = async event => {
    event.preventDefault();
    let today = moment().format("DD/MM/YY, h:mm:ss a");
    const data = {
      name: this.state.formControls.name.value,
      phone: this.state.formControls.phone.value,
      message: this.state.message.value,
      date: today
    };

    try {
      const response = await axios.post(reqURL.sendMessage, data);
      if (response.status === 201) {
        this.setState({ isSended: true });
        setTimeout(() => {
          this.setState(this._initState);
        }, 1000);
      }
    } catch (error) {
      console.log(error);
    }
  };

  render() {
    let closeModalIcon;
    if (this.props.modalContact) {
      closeModalIcon = (
        <i className="far fa-window-close modal-contact_close" onClick={this.props.closeModalContact}/>
      );
    }
    return (
      <div
        className={
          this.props.modalContact ? "ContactUs modalContact" : "ContactUs"
        }
      >
        {closeModalIcon}
        <h2>Связаться с нами</h2>
        <p>
          *Напишите свой номер телефона и мы вам перезвоним в ближайшее время
        </p>
        <div className="contact-inputs">
          <input
            tpye="text"
            className="contact-input"
            placeholder="Ваше имя(обязательный)"
            value={this.state.formControls.name.value}
            touched={this.state.formControls.name.touched.toString()}
            valid={this.state.formControls.name.valid.toString()}
            onChange={event => this.onChangeHandler(event, "name")}
          />
          <input
            tpye="text"
            className="contact-input"
            placeholder="Ваш телефон(обязательный)"
            value={this.state.formControls.phone.value}
            touched={this.state.formControls.phone.touched.toString()}
            valid={this.state.formControls.phone.valid.toString()}
            onChange={event => this.onChangeHandler(event, "phone")}
          />
        </div>

        <textarea
          id=""
          cols="30"
          rows="1"
          className="contact-text"
          placeholder="Ваше сообщение"
          value={this.state.message.value}
          onChange={event => this.textareaHandler(event, this.state.message)}
        />
        <button
          className="contact-btn"
          type="submit"
          onClick={this.sendMessageHandler}
        >
          Связаться
        </button>
        {this.state.isSended ? (
          <SnackbarComponent
            adminChange={this.state.isSended}
            open={this.state.isSended}
            message={`${
              this.state.formControls.name.value
            } сообшение отправленно`}
          />
        ) : null}
      </div>
    );
  }
}
