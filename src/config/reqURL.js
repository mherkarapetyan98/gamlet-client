export const domain = 'http://gamletstroy.ru'
export const reqURL = {
    'sendMessage': `${domain}/api/messages/`,
    'getMessages': `${domain}/api/messages/`,
    'deleteMessage': `${domain}/api/messages/`,
    'adminLogin': `${domain}/api/admin/login/`,
    'isLogged': `${domain}/api/admin/is-logged-in`,
    'createProduct': `${domain}/api/admin/`,
    'getCategory': `${domain}/api/category`,
    'getTrotuarAdmin': `${domain}/api/admin/`,
    'getTrotuar': `${domain}/api/product/`,
    'getProduct': `${domain}/api/product/category/`,
    'getProductsByCategory': `${domain}/api/product/`
}