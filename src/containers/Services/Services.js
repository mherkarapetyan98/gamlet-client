import React, { Component } from "react";
import "./Services.css";
import Headline from "../../components/UI/Headline/Headline";
import ZoomImage from "../../components/UI/ZoomImage/ZoomImage";
import ServicesSlider from "../../components/UI/sliders/servicesSlider/ServicesSlider";
import Axios from "axios";

let image;
let moreImages;

class Services extends Component {
  state = {
    isZoomed: false,
    redir: true,
    images: {
      ukladka: [
        {
          src: `${require("../../img/services/ukladka-examples/1.jpg")}`,
          alt: "ukladka1"
        },
        {
          src: `${require("../../img/services/ukladka-examples/2.jpg")}`,
          alt: "ukladka2"
        },
        {
          src: `${require("../../img/services/ukladka-examples/3.jpg")}`,
          alt: "ukladka3"
        },
        {
          src: `${require("../../img/services/ukladka-examples/4.jpg")}`,
          alt: "ukladka4"
        },
        {
          src: `${require("../../img/services/ukladka-examples/5.jpg")}`,
          alt: "ukladka5"
        },
        {
          src: `${require("../../img/services/ukladka-examples/6.jpg")}`,
          alt: "ukladka6"
        },
        {
          src: `${require("../../img/services/ukladka-examples/7.jpg")}`,
          alt: "ukladka7"
        },
        {
          src: `${require("../../img/services/ukladka-examples/8.jpg")}`,
          alt: "ukladka8"
        },
        {
          src: `${require("../../img/services/ukladka-examples/9.jpg")}`,
          alt: "ukladka9"
        }
      ],
      asphalt: [
        {
          src: `${require("../../img/services/asfalt/1.jpg")}`,
          alt: "asphalt1"
        },
        {
          src: `${require("../../img/services/asfalt/2.jpg")}`,
          alt: "asphalt2"
        },
        {
          src: `${require("../../img/services/asfalt/3.jpg")}`,
          alt: "asphalt3"
        },
        {
          src: `${require("../../img/services/asfalt/4.jpg")}`,
          alt: "asphalt4"
        },
        {
          src: `${require("../../img/services/asfalt/5.jpg")}`,
          alt: "asphalt5"
        },
        {
          src: `${require("../../img/services/asfalt/6.jpg")}`,
          alt: "asphalt6"
        }
      ],
      gazon: [
        { src: `${require("../../img/services/gazon/1.jpg")}`, alt: "gazon1" },
        { src: `${require("../../img/services/gazon/2.jpg")}`, alt: "gazon2" },
        { src: `${require("../../img/services/gazon/3.jpg")}`, alt: "gazon3" }
      ],
      kovka: [
        { src: `${require("../../img/services/kovka/1.jpg")}`, alt: "kovka1" },
        { src: `${require("../../img/services/kovka/2.jpg")}`, alt: "kovka2" },
        { src: `${require("../../img/services/kovka/3.jpg")}`, alt: "kovka3" },
        { src: `${require("../../img/services/kovka/4.jpg")}`, alt: "kovka4" },
        { src: `${require("../../img/services/kovka/5.jpg")}`, alt: "kovka5" },
        { src: `${require("../../img/services/kovka/6.jpg")}`, alt: "kovka6" } 
      ],
      arenda: [
        {
          src: `${require("../../img/services/arenda/1.jpg")}`,
          alt: "arenda1"
        },
        {
          src: `${require("../../img/services/arenda/2.jpg")}`,
          alt: "arenda2"
        },
        {
          src: `${require("../../img/services/arenda/3.jpg")}`,
          alt: "arenda3"
        },
        {
          src: `${require("../../img/services/arenda/4.jpeg")}`,
          alt: "arenda4"
        },
        {
          src: `${require("../../img/services/arenda/5.jpg")}`,
          alt: "arenda5"
        },
        {
          src: `${require("../../img/services/arenda/6.jpg")}`,
          alt: "arenda6"
        },
        { src: `${require("../../img/services/arenda/7.jpg")}`, alt: "arenda7" }
      ]
    }
  };

  renderImages = service => {
    return Object.keys(service).map(index => {
      const image = service[index];
      return (
        <div>
          <img
            key={index}
            src={image.src}
            alt={image.alt}
            id={image.alt}
            className="service__secondary-image"
            onClick={event => {
              this.zoomImageHandler(event, service);
            }}
            // onClick={event => {
            //   this.moreImagesSlider(event)
            // }}
          />
        </div>
      );
    });
  };

  zoomImageHandler = (event, service) => {
    this.setState({
      isZoomed: true
    });
    image = event.target;
    moreImages = service;
  };

  componentDidMount() {
    document.title = 'Услуги';
  }

  render() {
    let zoomImage;

    if (this.state.isZoomed) {
      zoomImage = (
        <ZoomImage
          isZoomed={this.state.isZoomed}
          imageSrc={image.src}
          selectedImage={image}
          imageAlt={image.alt}
          moreImages={moreImages}
          closeModal={() => this.setState({ isZoomed: false })}
        />
      );
      // zoomImage = (
      //   <ServicesSlider
      //     moreImages={moreImages}
      //   />
      // )
    }

   
    return (
      <div className="Services">
        <Headline title="УСЛУГИ" />
        <div className="service-block">
          <div className="service-block__images">
            <div>
              <img
                className="service__main-image"
                src={require("../../img/services/ukladka/main.jpg")}
                alt="УКЛАДКА ТРОТУАРНОЙ ПЛИТКИ"
              />
            </div>
            <div className="secondary-images">
              {this.renderImages(this.state.images.ukladka)}
            </div>
          </div>
          <div className="service-block__description">
            <h2>УКЛАДКА ТРОТУАРНОЙ ПЛИТКИ</h2>
            <p>
              Для того, чтобы сделать архитектуру участка индивидуальной и
              красивой используют множество способов укладки тротуарной плитки и
              широкую цветовую гамму. Тротуарная плитка хорошего качества
              устойчива к плохим погодным условиям, а ее долговечность напрямую
              зависит от качества материала и способа укладки.
            </p>
            <p
              style={{
                color: "red",
                fontSize: "1.3rem",
                textAlign: "center"
              }}
            >
              Работа "под ключ" цена 1000-2500 руб. за 1 кв.м
            </p>{" "}
            <p>
              Правильная укладка тротуарной плитки, которая соответствует
              технологии и качеству покрытия, является залогом его
              долговечности, безопасности и красоты ландшафта загородного дома
              или территории офиса. Мощение тротуарной плитки для сильно
              нагруженных дорожек производится на бетонное основание, для
              пешеходных и садовых дорожек достаточно использования
              песчано-гравийной или песчаной подушки. Разная трудоемкость работ
              по подготовке и закладке основания различных типов оказывает
              влияние на итоговую стоимость работ. Дизайн-проект обустройства
              участка предусматривает изучение и обследование территории и
              свойств грунта, а также планирование расположения садовых,
              пешеходных и подъездных дорожек, выбор материала и составление
              сметы на укладку тротуарной плитки. Цена формируется, исходя из
              объема работ, типа основания, необходимости установки бордюра или
              прокладки дренажа.
            </p>
          </div>
        </div>

        <div className="service-block">
          <div className="service-block__images">
            <div>
              <img
                className="service__main-image"
                src={require("../../img/services/arenda/big.jpg")}
                alt="АСФАЛЬТИРОВАНИЕ ДОРОГ"
              />
            </div>
            <div className="secondary-images">
              {this.renderImages(this.state.images.arenda)}
            </div>
          </div>
          <div className="service-block__description">
            <h2>Выгодная аренда спецтехники</h2>
            <p>
              Неважно, где находится ваша стройка или другой объект, требующий
              работы спецтехники. Мы оперативно доставим технику, если вам нужно
              выполнить следующие работы:
            </p>
            <ul style={{ marginLeft: "25px", listStyle: "circle" }}>
              <li>строительные</li>
              <li>дорожные</li>
              <li>снегоуборочные</li>
              <li>землеройные</li>
            </ul>
            <p
              style={{
                color: "red",
                fontWeight: "bold",
                fontSize: "1.2rem",
                textAlign: "center"
              }}
            >
              Цены договорные
            </p>
          </div>
        </div>

        <div className="service-block">
          <div className="service-block__images">
            <div>
              <img
                className="service__main-image"
                src={require("../../img/services/asfalt/main.jpg")}
                alt="АСФАЛЬТИРОВАНИЕ ДОРОГ"
              />
            </div>
            <div className="secondary-images">
              {this.renderImages(this.state.images.asphalt)}
            </div>
          </div>
          <div className="service-block__description">
            <h2>АСФАЛЬТИРОВАНИЕ ДОРОГ</h2>
            <p>
              Асфальтирование дорог - это не просто укладка асфальта. Вначале
              ведутся дорожно-строительные работы по укреплению и стабилизации
              грунта. Еще мы сделаем работы для маленьких площадей. Если эти
              работы по произведены правильно, покрытие будет служить долго.
              Вещества, являющиеся эффективными вяжущими агентами, начинают
              действовать за счёт химического строения грунта, которые
              превращают грунт в камень и улучшают его качество. Для повышения
              прочности дорожных покрытий всё чаще применяются вещества,
              склеивающие частицы грунта.
            </p>
          </div>
        </div>

        <div className="service-block">
          <div className="service-block__images">
            <div>
              <img
                className="service__main-image"
                src={require("../../img/services/gazon/main.jpg")}
                alt="ОБСЛУЖИВАНИЕ ГАЗОНОВ"
              />
            </div>
            <div className="secondary-images">
              {this.renderImages(this.state.images.gazon)}
            </div>
          </div>
          <div className="service-block__description">
            <h2>ОБСЛУЖИВАНИЕ ГАЗОНОВ</h2>
            <p>
              Сервис газонов - это комплексная услуга, которая включает в себя
              все виды сезонных и регулярных работ. Наши специалисты обслуживают
              газоны в на территориях офисных зданий и на загородных участках.
              Они осуществляют уход за любыми видами натуральных газонов, в т.ч.
              за самыми "трудными" - спортивными и партерными.
            </p>
          </div>
        </div>

        <div className="service-block with-video-block">
          <div className="with-video">
            <div className="service-block__images">
              <div>
                <img
                  className="service__main-image"
                  src={require("../../img/services/kovka/kovka.jpg")}
                  alt="ХУДОЖЕСТВЕННАЯ КОВКА"
                />
              </div>
              <div className="secondary-images">
                {this.renderImages(this.state.images.kovka)}
              </div>
            </div>
            <div className="service-block__description">
              <h2>ХУДОЖЕСТВЕННАЯ КОВКА</h2>
              <p>
                Художественная ковка особенно незаменима для загородных
                особняков и коттеджей. Их атмосфера как бы «заранее» проникнута
                роскошью и несравненным комфортом. Здесь необходимо грамотным
                образом подчеркнуть индивидуальный стиль и презентабельность
                хозяина. Наши мастера помогут Вам украсить свой роскошный
                загородный дом ажурными лестничными перилами, миниатюрными
                мебельными предметами многими другими шедеврами ковки. Все
                подбирается индивидуально для каждого интерьера в самом
                оптимальном стиле.
              </p>
            </div>
          </div>
          <div className="video_block">
            <iframe
              width="100%"
              height="400"
              src="https://www.youtube.com/embed/AVt0VQLiD2Y"
              frameborder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowfullscreen
            />
          </div>
          <div className="video_block-images">
            <img src={require("../../img/services/kovka/7.jpg")} alt="kovka" />
            <img src={require("../../img/services/kovka/8.jpg")} alt="kovka" />
          </div>
        </div>
        {zoomImage}
      </div>
    );
  }
}
export default Services;
