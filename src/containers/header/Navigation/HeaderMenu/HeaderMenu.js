import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import "./HeaderMenu.css";
import MenuToggle from "../../../../components/Navigation/MenuToggle/MenuToggle";
import BackDrop from "../../../../components/UI/BackDrop/BackDrop";

const links = [
  { to: "/", label: "Главная", exact: true },
  { to: "/products", label: "Продукты", exact: true },
  { to: "/services", label: "Услуги", exact: true },
  { to: "/reviews", label: "Отзывы", exact: true },
  { to: "/contact", label: "Контакты", exact: true }
];

class HeaderMenu extends Component {
  state = {
    menu: false
  };

  toggleMenuHandler = event => {
    this.setState({
      menu: !this.state.menu
    });
  };

  menuCloseHandler = event => {
    this.setState({
      menu: false
    });
  };

  renderLinks() {
    return links.map((link, index) => {
      return (
        <li key={index}>
          <NavLink
            to={link.to}
            exact={link.exact}
            onClick={this.menuCloseHandler}
            activeStyle={{ color: '#083537', background: '#f0f0f0'}}
          >
            {link.label}
          </NavLink>
        </li>
      );
    });
  }

  render() {
    let cls = "Drawer";
    if (!this.state.menu) {
      cls = "Drawer close";
      document.querySelector("body").style.overflow = "visible";
    } else {
      document.querySelector("body").style.overflow = "hidden";
    }
    return (
      <React.Fragment>
        <nav className="HeaderMenu">
          <ul>{this.renderLinks()}</ul>
        </nav>
        <MenuToggle
          onToggle={this.toggleMenuHandler}
          isOpen={this.state.menu}
        />
        <React.Fragment>
          <nav className={cls}>          
            <ul>{this.renderLinks()}</ul>
          </nav>
        </React.Fragment>

        {this.state.menu ? <BackDrop onClick={this.menuCloseHandler} /> : null}
      </React.Fragment>
    );
  }
}

export default HeaderMenu;
