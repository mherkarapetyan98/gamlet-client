import React, { Component } from "react";
import HeaderMenu from "./HeaderMenu/HeaderMenu"; 
import "./Navigation.css";

class Navigation extends Component {

  render() {
    return (
      <div className="Navigation">
           {/* nav-menu */}
          <HeaderMenu />
        
      </div>
    );
  }
}

export default Navigation;
