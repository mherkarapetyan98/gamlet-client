import React, { Component } from "react";
import "./Header.css";
import Navigation from "./Navigation/Navigation";
import CallModal from "../../components/Modal/CallModal";

export default class Header extends Component {
  state = {
    modalOpen: false
  }
  openModalHandler = () => { 
    this.setState({
      modalOpen: true
    })
  }
  render() {
    return (
      <div className="Header">
        <div className="header-top">
          <div className="contact-phone">
            <a href="tel:+79169057933">8-916-905-79-33</a>
          </div>
          <div className="contact-mail">
            <a href="mailto:info@gamletstroy.ru">info@gamletstroy.ru</a>
          </div>
        </div>
        <div className="header-about">
          <div className="logo">
            <a href="http://gamletstroy.ru">
              <img src={require("../../img/UI/Logo.png")} alt="GamletStroy" />
            </a>
          </div>

          <div className="header-title">             
            <h1>Укладка тротуарной плитки под ключ</h1>
            <button className="order-button" onClick={this.openModalHandler}>Заказать звонок</button>
            <div className="order-text">*Это бесплатно и занимает всего пару минут!</div>
            {this.state.modalOpen ? <CallModal closeModalContact={() => this.setState({modalOpen: false})} /> : null }
          </div>
        </div>
        <Navigation />
      </div>
    );
  }
}
