import React, { Component } from "react";
import Headline from "../../components/UI/Headline/Headline";
import { Route, Switch, withRouter, Redirect } from "react-router-dom";
import axios from "axios";
import { reqURL } from "../../config/reqURL";
import ProductCard from "./content/ProductCard";
import "./Products.css";

let currentCategory = ''
export default class Products extends Component {
  state = {
    value: 0,
    categories: [],
    currentCategory: ""
  };
  async componentDidMount() {
    document.title = "Продукты ";
    try {
      const response = await axios.get(reqURL.getCategory); 
      const categories = response.data;

 
      this.setState({
        categories
      });
    } catch (error) {
      console.log(error);
    } 
  }

 
  render() {
    const { categories } = this.state;
    let currentCategoryKey = document.location.href.split('/')[4]
    
    return (
      <div className="Products">
        {categories.map(category => {
          if(category.key === currentCategoryKey) {
            return <Headline title={category.name || 'Продукты'} key={category.name} />
          }
        })}
        <Switch>
          {categories.map(category => { 
            return (
              <Route
                path={`/products/${category._id}`}
                exact
                key={category.key}
                render={props => (
                  <ProductCard
                    {...props}
                    categoryKey={category.key}
                    categoryId={category._id}
                    key={category.key}
                    name={category.name}
                  />
                )}
              />
              
            );
          })}     
          <Route path={`/products`} render={() => <Redirect to="/products/5ca7bde60bb6f7100d573bf1"/>} exact/>    
        </Switch>
      </div>
    );
  }
}
