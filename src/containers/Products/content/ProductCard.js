import React, { Component } from "react";
import "./ProductCard.css";
import axios from "axios";
import {NavLink} from 'react-router-dom';
import Loader from "../../../components/UI/Loader/Loader";
import { reqURL } from "../../../config/reqURL";

export default class ProductCard extends Component {
  state = {
    pages: "",
    page: "",
    next: "",
    prev: "",
    trotuar: [],
    loading: true
  };

  async componentDidMount() {
    document.title = `${
      this.props.name
    } Воскресенский район, Раменский район, Домодедовский район`;
    try {
      console.log(this.props.categoryKey)
      const response = await axios.get(
        // `http://localhost:5000/api/admin/${this.props.categoryKey}`
        `http://gamletstroy.ru/api/product/${this.props.categoryId}`
      );
      const trotuar = this.state.trotuar.concat(); 
      Object.keys(response.data.products).map(index => {
        trotuar.push(response.data.products[index]);
      });

      this.setState({
        pages: response.data.pages,
        page: response.data.page,
        next: response.data.next,
        prev: response.data.prev,
        trotuar,
        loading: false
      });
    } catch (error) {
      console.log(error);
    }
  }

  renderProductCards = () => {
    return Object.keys(this.state.trotuar).map(index => {
      const product = this.state.trotuar[index]; 
      return (
        <div className="product-card" key={`${product._id}`}>
          <div className="card__img">
            <h4>{product.name}</h4>
            <img
              src={`http://plitt.ru:5000/uploads/${product.imageName}`}
              alt={product.name}
            />
          </div>
          <div className="card__about">
            {/* <h4>Волна</h4> */}
            <div className="about__table">
              <div className="divTable">
                <div className="divTableBody">
                  <div className="divTableRow">
                    {!!product.size ? (
                      <div className="divTableCell divTableHeading">
                        Размер (мм)
                      </div>
                    ) : (
                      <br />
                    )}

                    <div className="divTableCell">{product.size}</div>
                  </div>
                  <div className="divTableRow">
                    {!!product.height ? (
                      <div className="divTableCell divTableHeading">
                        Высота (мм)
                      </div>
                    ) : (
                      <br />
                    )}
                    <div className="divTableCell">{product.height}</div>
                  </div>
                  <div className="divTableRow">
                    {!!product.quantity ? (
                      <div className="divTableCell divTableHeading">
                        Кол-во штук в 1м²
                      </div>
                    ) : (
                      <br />
                    )}
                    <div className="divTableCell">{product.quantity}</div>
                  </div>
                  <div className="divTableRow">
                    {!!product.process ? (
                      <div className="divTableCell divTableHeading">
                        Способ изготовления
                      </div>
                    ) : (
                      <br />
                    )}
                    <div className="divTableCell">{product.process}</div>
                  </div>
                </div>
              </div>
              <div className="fix-bottom-card">
                <div>Цена за 1м²</div>
                <div className="card-price">
                  <div className="card-price__gray">
                    <div>Серая</div>
                    <div>{product.price}руб.</div>
                  </div>
                  <div className="card-price__color">
                    <div>Цветная</div>
                    <div>от {product.priceColor}руб.</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      );
    });
  };

  pagesHandler = async (event,number) => {
    event.preventDefault()  
    try { 
      const url = `http://gamletstroy.ru/api/product/${
        this.props.categoryId
      }?page=${number}`;
      const response = await axios.get(url);
      window.scrollTo(0, 250)
      this.setState({
        pages: response.data.pages,
        page: response.data.page,
        next: response.data.next,
        prev: response.data.prev,
        trotuar: response.data.products
      });
    } catch (error) {
      console.log(error);
    }
  };

  renderPagination = () => {
    const pages = this.state.pages; 
    const numbers = [];
    for (let i = 1; i <= pages; i++) {
      numbers.push(i);
    }
    return numbers.map(number => {    
      return (
        <li key={number} className={number === this.state.page? 'active_page' : ''} onClick={(event) => this.pagesHandler(event, number)}>          
          {number}  
        </li>
      );
    });
  };

  render() {
    return (
      <React.Fragment>
        <div className="all-cards">
          {this.state.loading ? <Loader /> : this.renderProductCards()}
        </div>
        <div className="pagination-block">
          <ul>{this.renderPagination()}</ul>
        </div>
      </React.Fragment>
    );
  }
}
