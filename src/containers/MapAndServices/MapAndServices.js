import React, { Component } from 'react';
import './MapAndServices.css';
import MapComponent from '../../components/Map/Map'; 
import ContactUs from '../../components/ContactUs/ContactUs';

export default class MapAndServices extends Component {
  render() {
    return (
      <div className="MapAndServices">   
        <div className="footer-services">
          <div className="footer-service">
              <img className="service-img"
              src={require('../../img/UI/service-icons/resize.png')} alt="resize"/>
              <p>С нашей стороны проводится изучение местности и обмерные работы</p>
          </div>
          <div className="footer-service">
              <img className="service-img" src={require('../../img/UI/service-icons/worker.png')} alt="worker"/>
              <p>Мы делаем различные строительные работы</p>
          </div>
          <div className="footer-service">
              <img  className="service-img" src={require('../../img/UI/service-icons/truck.png')} alt="truck"/>
              <p>Быстрая доставка по указанному вами адресу</p>
          </div>
        </div>
      </div>
    )
  }
}
