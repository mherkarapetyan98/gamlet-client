import React, { Component } from "react";
import "./Contact.css";
import Headline from "../../components/UI/Headline/Headline";

export default class Contact extends Component {
  render() {
    return (
      <div className="Contact">
        <Headline title="КОНТАКТЫ" />
        <div className="contact-block">
          <div className="contact__phone">
            <h4>Номер телефона</h4>
            {/* <p>8-926-530-33-99</p> */}
            <p>8-916-905-79-33</p>
            <p>8-985-412-92-53</p>
            <p>8-917-580-70-90</p>
            <div>
              <i
                className="fab fa-viber"
                style={{ color: "#59267c", fontSize: "1.1rem" }}
              />{" "}
              <i
                className="fab fa-whatsapp"
                style={{ color: "#075e54", fontSize: "1.1rem" }}
              />
            </div>
          </div>
          <div className="contact__address">
            <h4>Адрес</h4>
            <p>Московская обл., г. Подольск, Домодедовское шоссе, 37</p>
            <p>Принимаем заказы по следующим районам: Домодедово, Подольск, Чехов, Бронницы.</p>
            <p>Работаем без выходных с 8:00 по 22:00</p>
          </div>
        </div>
      </div>
    );
  }
}
