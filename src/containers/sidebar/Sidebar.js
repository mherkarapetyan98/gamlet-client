import React, { Component } from "react";
import "./Sidebar.css";
import { reqURL } from "../../config/reqURL";
import { Route, Switch, withRouter, NavLink } from "react-router-dom";
import axios from "axios";
import Products from "../Products/Products";

export default class Sidebar extends Component {
  state = {
    value: 0,
    categories: []
  };
  async componentDidMount() {
    document.title = "Продукты ";
    try {
      const response = await axios.get(reqURL.getCategory);
      const categories = response.data;
      this.setState({
        categories
      });
    } catch (error) {
      console.log(error);
    }
  }

  scrollToEndOfSidebarHandler = () => {
    const mq = window.matchMedia("(max-width: 1100px)");
    if (mq.matches) {
      this.EndOfSidebar.scrollIntoView({ behavior: "smooth" });
    }
  };

  renderSidebarCatalog = () => {
    return this.state.categories.map(category => {
      return (
        <NavLink
          key={category._id}
          to={`/products/${category._id}`}
          exact={true}
          activeStyle={{ color: "#e7e7e7" }}
          style={{ color: "#fff", textDecoration: "none" }}
          onClick={() => this.scrollToEndOfSidebarHandler()}
        >
          <div className="catalog-row">{category.name}</div>
        </NavLink>
      );
    });
  };

  render() {
    return (
      <div
        className="Sidebar"
        ref={el => {
          this.Sidebar = el;
        }}
      >
        <div className="sidebar__catalog">
          <div className="catalog-header">Каталог</div>
          {this.renderSidebarCatalog()}
          <NavLink
            to={`/materials`}
            exact={true}
            activeStyle={{ background: "#e7e7e7" }}
            style={{ color: "#fff", textDecoration: "none" }}
            onClick={() => this.scrollToEndOfSidebarHandler()}
          >
            <div className="catalog-row">Стройматериалы</div>
          </NavLink>
          <NavLink
            to={`/services`}
            exact={true}
            activeStyle={{ background: "#e7e7e7" }}
            style={{ color: "#fff", textDecoration: "none" }}
            onClick={() => this.scrollToEndOfSidebarHandler()}
          >
            <div className="catalog-row">Услуги</div>
          </NavLink>
        </div>

        <div className="sidebar__text">
          <div
            style={{ display: "flex", marginTop: "10px", alignItems: "center" }}
          >
            <i
              className="fas fa-map-marker-alt"
              style={{ marginRight: "5px" }}
            />
            <p style={{ padding: "5px", fontWeight: "bold" }}>
              Принимаем заказы по следующим районам: Домодедово, Подольск,
              Чехов, Бронницы.
            </p>
          </div>

          <div
            style={{ display: "flex", marginTop: "10px", alignItems: "center" }}
          >
            <i className="far fa-clock" style={{ marginRight: "5px" }} />
            <p style={{ padding: "5px", fontWeight: "bold" }}>
              Работаем без выходных с 8:00 по 22:00
            </p>
          </div>
        </div>
        <div
          style={{ display: "flex", marginTop: "25px", alignItems: "center" }}
        >
          <iframe
            width="250"
            height="174"
            src="https://www.youtube.com/embed/bcptHjQWzE0?rel=0"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen
          />
        </div>
        <div
          ref={el => {
            this.EndOfSidebar = el;
          }}
        />
      </div>
    );
  }
}
