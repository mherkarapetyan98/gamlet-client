import React, { Component } from "react";
import "./Main.css";
// import MainSlider from '../../components/Slider/MainSlider'
import MainSlider from "../../components/Slider/MainSlider";
import ServiceTile from "../../components/ServiceTile/ServiceTile";
import ContactUs from "../../components/ContactUs/ContactUs";

export default class Main extends Component {
  state = {
    serviceTiles: [
      {
        imgSrc: require("../../img/service-tile/1.jpg"),
        title: "АСФАЛЬТИРОВАНИЕ ДОРОГ"
      },
      {
        imgSrc: require("../../img/service-tile/2.jpg"),
        title: "УКЛАДКА ТРОТУАРНОЙ ПЛИТКИ"
      },
      {
        imgSrc: require("../../img/service-tile/3.jpg"),
        title: "АРЕНДА СПЕЦТЕХНИКИ"
      },
      {
        imgSrc: require("../../img/service-tile/4.jpg"),
        title: "ХУДОЖЕСТВЕННАЯ КОВКА"
      },
      {
        imgSrc: require("../../img/service-tile/5.jpg"),
        title: "Обслуживание газонов"
      }
    ]
  };
  renderServices = () => {
    return Object.keys(this.state.serviceTiles).map(index => {
      const service = this.state.serviceTiles[index]
      return <ServiceTile key={index} image={service.imgSrc} title={service.title}/>;

    })
  };

  componentDidMount() {
    document.title = 'Укладка тротуарной плитки под ключ';
  }
  

  render() {
    return (
      <div className="Main">
        <div>
          <MainSlider />
        </div>
     

        <div className="service-tiles">{this.renderServices()}</div>
    
      </div>
    );
  }
}
