import React, { Component } from "react";
import Navigation from "./Navigation/Navigation";
import "./Footer.css";

export default class Footer extends Component {
  render() {
    return (
      <div className="Footer">
        <div className="footer-navigation">
          <div className="footer-contacts">
            <div className="footer_contact-phone footer-contact_item">
              <i className="fas fa-phone-volume" />
              <a href="tel:+79265303399">8-926-530-33-99</a>
            </div>
            <div className="footer_contact-address footer-contact_item">
              <i className="fas fa-map-marker-alt" />
              <p>
                Московская обл., г. Подольск, Домодедовское шоссе, 37 <br />
                Раменский район, деревня Жирошкино
              </p>
            </div>
            <div className="footer_contact-mail footer-contact_item">
              <i className="fas fa-envelope" />
              <a href="mailto:info@gamletstroy.ru">info@gamletstroy.ru</a>
            </div>
          </div>
          <Navigation />
        </div>
      </div>
    );
  }
}
