import React, { Component } from 'react';
import { NavLink } from "react-router-dom";
import './FooterMenu.css';

const links = [
  { to: "/", label: "Главная", exact: true },
  { to: "/products", label: "Продукты", exact: false },
  { to: "/services", label: "Услуги", exact: false },
  { to: "/contact", label: "Контакты", exact: false }
];


class FooterMenu extends Component {

  renderLinks() {
    return links.map((link, index) => {
      return (
        <li key={index}>
          <NavLink
            to={link.to}
            exact={link.exact}  
          >
          {link.label}
          </NavLink>
        </li>
      );
    });
  }
 
  render() {
    return (
      <nav className="FooterMenu">
         <ul>{this.renderLinks()}</ul>
      </nav>
    )
  }
}

export default FooterMenu;