import React, { Component } from "react";
import { Route, Switch } from "react-router-dom";
import "./App.css";
import PublicLayout from "./hoc/Layout/PublicLayout";
import Admin from "./components/admin/Admin";
import Login from "./components/admin/containers/Login/Login";

class App extends Component {
  render() {
    return (
      <div className="App">
        <Switch>
          <Route path="/admin/login" component={Login} exact />
          <Route path="/admin" component={Admin} />
          <Route path="/" component={PublicLayout} />
        </Switch>
      </div>
    );
  }
}

export default App;
