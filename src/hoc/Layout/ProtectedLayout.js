import React from "react";
import { Route, Switch } from "react-router-dom";

import Admin from "../../components/admin/Admin";
import Login from "../../components/admin/containers/Login/Login";
import CreateProduct from "../../components/admin/components/Main/CreateProduct/CreateProduct";
import ProductsList from "../../components/admin/components/Main/ProductsList/ProductsList";

const ProtectedLayout = () => {
  return (
    <div>
      <Switch>
        <Route path="/admin/products" component={ProductsList} exact />
        <Route path="/admin/create" component={CreateProduct} exact />
        <Route path="/admin/login" component={Login} exact />
        <Route path="/admin" component={Admin} exact />
      </Switch>
    </div>
  );
};

export default ProtectedLayout;
