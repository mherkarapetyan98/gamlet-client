import React, { Component } from "react";
import "./PublicLayout.css";
import { Route, Switch } from "react-router-dom";
import Header from "../../containers/header/Header";
import Sidebar from "../../containers/sidebar/Sidebar";
import Main from "../../containers/Main/Main";
import ContactUs from "../../components/ContactUs/ContactUs";
import Footer from "../../containers/footer/Footer";
import MapAndServices from "../../containers/MapAndServices/MapAndServices";
import MapComponent from "../../components/Map/Map";
import Products from "../../containers/Products/Products";
import Services from "../../containers/Services/Services";
import Reviews from "../../containers/Reviews/Reviews";
import Contact from "../../containers/Contact/Contact";
import Materials from "../../containers/Materials/Materials"

export default class componentName extends Component {
  render() {
    return (
      <div>
        {/* Part for header */}
        <Header />
        {/* ************** body routes */}
        <div className="main-wrapper">
          <Sidebar />
          <div className="main-content">
            <Switch>
              <Route path="/contact" component={Contact} />
              <Route path="/reviews" component={Reviews} />
              <Route path="/services" component={Services} />
              <Route path="/materials" component={Materials} />
              <Route path="/products" component={Products} />
              <Route path="/" component={Main} />
            </Switch>
          </div>
        </div>
        {document.URL === "http://gamletstroy.ru/" ||
        document.URL === "http://gamletstroy.ru/contact" ? (
          <React.Fragment>
            <ContactUs /> <MapComponent />{" "}
          </React.Fragment>
        ) : null}
        {/* Map part and some services */}

        <MapAndServices />
        {/* Part for footer */}
        <Footer />
      </div>
    );
  }
}
