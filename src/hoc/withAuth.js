import React from 'react';
import { Redirect } from 'react-router-dom';
import { reqURL } from '../config/reqURL';
// import axios from 'axios';

export default function withAuth(Component) {
    return class Authenticated extends React.Component {
        constructor(props) {
            super(props);
            this.state = { isLogged: false, isLoading: true };
        }
        async isLoggedIn() {
            const token = localStorage.getItem('token');

            if (token !== null) {
                const options = {
                    method: 'POST',
                    headers: {
                        'Authorization': `${token}`
                    }
                };

                return fetch(reqURL.isLogged, options)
                    .then(response => response.json())
                    .then(responseJson => responseJson)
                    .catch(error => console.log(error));
            }
            return false
        }

        async componentDidMount() {

            if (await this.isLoggedIn()) {

                this.setState({ isLogged: true, isLoading: false })


            } else {
                console.log('not logged in');
                this.setState({ isLogged: false, isLoading: false });
            }
        }

        render() {
            return (
                <div>
                    {this.state.isLoading
                        ? 'loading'
                        : this.state.isLogged ? <Component {...this.props} /> : <Redirect to="/" />}
                </div>

            );
        }
    }
}